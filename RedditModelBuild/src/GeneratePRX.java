import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import com.google.common.base.Splitter;

import pitt.search.semanticvectors.FlagConfig;
import pitt.search.semanticvectors.LuceneUtils;
import pitt.search.semanticvectors.VectorStoreRAM;
import pitt.search.semanticvectors.vectors.RealVector;
import pitt.search.semanticvectors.vectors.Vector;
import pitt.search.semanticvectors.vectors.VectorFactory;
import pitt.search.semanticvectors.vectors.VectorType;

public class GeneratePRX {

    //public static String VECTOR_FILE = "hal-10/drxntermvectors.bin";
    //public static String exportFileName = "hal-10-consumers";
    //public static String exportFileName = "hal-10-experts";
    //public static String exportFileName = "hal-10-lexfree";
    //public static String VECTOR_FILE = "default-10/termtermvectors.bin";
    //public static String exportFileName = "default-10-consumers";
    //public static String exportFileName = "default-10-experts";
    //public static String exportFileName = "default-10-lexfree";
    //public static String VECTOR_FILE = "index/termvectors.bin";
    /*public static String exportFileName = "10-consumers";*/
    //public static String exportFileName = "10-experts";
    //public static String exportFileName = "10-lexfree";
    //public static String VECTOR_FILE = "embeddings/embeddingvectors.bin";
    //public static String exportFileName = "embed-10-consumers";
    //public static String exportFileName = "embed-10-experts";
    //public static String exportFileName = "embed-10-lexfree";
    /**
     * DRRI Consumer *
     */
    //public static String VECTOR_FILE = "consumer-drri/termvectors2.bin";
    //public static String exportFileName = "consumer-drri";
    /**
     * TRRI Consumer*
     */
    /*public static String VECTOR_FILE = "consumer-trri/termvectors2.bin";
	public static String exportFileName = "consumer-trri";*/
    /**
     * DRRI Expert *
     */
    /*public static String VECTOR_FILE = "expert-drri/termvectors2.bin";
	public static String exportFileName = "expert-drri";*/
    /**
     * TRRI Expert *
     */
    public static String VECTOR_FILE = "/Users/mfamith/Desktop/reddit-files/trri/termvectors2.bin";
    public static String exportFileName = "trri";

    //embedding
    ///Users/mfamith/Desktop/reddit-files/skipgram/embeddingvectors.bin
    //drri
    ///Users/mfamith/Desktop/reddit-files/drri/termvectors2.bin
    //directional (HAL)
    ///Users/mfamith/Desktop/reddit-files/directional/drxntermvectors.bin
    //termxdoc
    ///Users/mfamith/Desktop/reddit-files/termxdoc/termvectors.bin
    //sliding window
    // /Users/mfamith/Desktop/reddit-files/slidingwindow/termtermvectors.bin
    /**
     * Expert Resources *
     */
    //public static String VECTOR_FILE = "embeddings-expert/embeddingvectors.bin";
    //public static String exportFileName = "embed-10-consumers";
    //public static String exportFileName = "embed-expertdocs-10-experts";
    //
    public static String TERMS = "terms.txt";
    

    private static Map<String, Vector> termCollection = new LinkedHashMap<String, Vector>();

    public static void main(String[] args) {

        System.out.println("***Processing started****");
        FlagConfig defaultFlagConfig = FlagConfig.getFlagConfig(null);

        readTermFile(TERMS);

        try {
            VectorStoreRAM searchVectorStore = VectorStoreRAM.readFromFile(defaultFlagConfig, VECTOR_FILE);

            LuceneUtils luceneUtils = null;

            for (Map.Entry<String, Vector> entry : termCollection.entrySet()) {
                if (entry.getKey().contains(" ")) {
                    //break multiword term concept
                    Splitter spaceSplitter = Splitter.on(" ").omitEmptyStrings().trimResults();
                    Iterable<String> tokens = spaceSplitter.split(entry.getKey());
                    entry.setValue(HandleMultiWordTerms(tokens, searchVectorStore));
                } else {
                    Vector searchVector = searchVectorStore.getVector(entry.getKey());
                    entry.setValue(searchVector);
                }

            }

            float min = 100000;
            float max = -100000;

            //Vector output
            String vectorOutput = "";
            int dimension = 5;
            ArrayList<String> final_terms = new ArrayList<String>();
            for (Map.Entry<String, Vector> entry : termCollection.entrySet()) {
                //System.out.println("for term: " + entry.getKey());
                //System.out.println("its value: " + entry.getValue().writeToString());
                /*if(entry.getValue()==null){
					entry.setValue(VectorFactory.createZeroVector(VectorType.REAL, 200));
					//System.out.println("for dimension: " + entry.getValue().writeToString());
				}*/

                if (entry.getValue() != null) {
                    dimension = entry.getValue().getDimension();

                    for (int i = 0; i < dimension; i++) {
                        vectorOutput = vectorOutput.concat(Float.toString(((RealVector) entry.getValue()).getCoordinates()[i]) + " ");
                        min = Math.min(((RealVector) entry.getValue()).getCoordinates()[i], min);
                        max = Math.max(((RealVector) entry.getValue()).getCoordinates()[i], max);
                    }
                    vectorOutput = vectorOutput.concat("\r\n");
                    final_terms.add(entry.getKey());
                }

            }

            /*String headerTemplate = "data\r\nsimilarity\r\n" + termCollection.size() + " nodes\r\n" +"9 decimal places\r\n"
					+ min +" minimum data value\r\n" + max + " maximum data value\r\ncoord\r\n" + dimension +" dimensions\r\n"
					//+ "Euclidean Standardized\r\n";
					+ "City Block Standard\r\n";*/
            String headerTemplate = "data\r\nsimilarity\r\n" + final_terms.size() + " nodes\r\n" + "9 decimal places\r\n"
                    + min + " minimum data value\r\n" + max + " maximum data value\r\ncoord\r\n" + dimension + " dimensions\r\n"
                    + "Euclidean Standard\r\n";
            //+ "City Block Standard\r\n";
            String finalOutput = headerTemplate.concat(vectorOutput);

            //System.out.println(finalOutput);
            outputFile(finalOutput, exportFileName, final_terms);

            System.out.println("***Processing completed****");

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    public static void outputFile(String content, String filename, ArrayList<String> terms) throws FileNotFoundException, IOException {

        File file = new File(filename + ".prx.txt");
        try (FileOutputStream fop = new FileOutputStream(file)) {

            byte[] contentInBytes = content.getBytes();
            fop.write(contentInBytes);
            fop.flush();
            fop.close();
        }
        FileWriter writer = new FileWriter("terms_" + filename + ".txt");
        for (String term : terms) {
            writer.write(term + "\n");
        }
        writer.close();

    }

    public static void readTermFile(String fileNamePath) {
        try {
            FileReader fr = new FileReader(fileNamePath);
            BufferedReader bf = new BufferedReader(fr);

            String line = "";

            while ((line = bf.readLine()) != null) {
                termCollection.put(line, null);
            }
            //System.out.println("Size of term collection is " + termCollection.size());

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    public static Vector HandleMultiWordTerms(Iterable<String> terms, VectorStoreRAM searchVectorStore) {

        Map<String, Vector> termRecord = new LinkedHashMap<String, Vector>();

        for (String term : terms) {
            termRecord.put(term, searchVectorStore.getVector(term.trim()));
        }

        String primaryKey = "";
        Vector primaryVector = null;
        for (Map.Entry<String, Vector> entry : termRecord.entrySet()) {

            if (primaryVector == null) {
                primaryKey = entry.getKey();
                //System.out.println(primaryKey);

                if (entry.getValue() == null) {
                    //System.out.println(primaryKey);
                    entry.setValue(VectorFactory.createZeroVector(VectorType.REAL, 200));
                    //System.out.println("for dimension: " + entry.getValue().writeToString());
                }
                primaryVector = (Vector) entry.getValue().copy();

                //VectorFactory.createZeroVector(VectorType.REAL, 200)
            } else {
                if (entry.getValue() != null) {

                    primaryVector.superpose(entry.getValue(), 1, null);
                    primaryVector.normalize();
                }

            }

        }

        return primaryVector;
    }

}

