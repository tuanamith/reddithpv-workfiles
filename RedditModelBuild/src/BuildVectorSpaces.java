import java.io.IOException;

import pitt.search.semanticvectors.BuildIndex;
import pitt.search.semanticvectors.BuildPositionalIndex;



public class BuildVectorSpaces {

	

	public BuildVectorSpaces() {
		// TODO Auto-generated constructor stub
		
		
	}
	
	/****** TRRI *******/
	
	
	public void buildTRRI(String luceneIndexPath,String documentModelName, String termModelName, int cycles) {
		
	
		try {
			BuildIndex.main(new String [] {"-luceneindexpath", luceneIndexPath, "-trainingcycles", 
					Integer.toString(cycles), "-initialtermvectors", "random",
					"-termvectorsfile", termModelName, "-docvectorsfile", documentModelName});
		} catch (IllegalArgumentException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void buildTRRI(String luceneIndexPath,String documentModelName, String termModelName) {

		
		try {
			
			BuildIndex.main(new String [] {"-luceneindexpath", luceneIndexPath, 
					"-trainingcycles", "2", "-initialtermvectors", "random",
					"-termvectorsfile", termModelName, "-docvectorsfile", documentModelName
					});
		} catch (IllegalArgumentException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	
	/*** DRRI ****/
	
	
	public void buildDRRI(String luceneIndexPath, String documentModelName, String termModelName, int cycles) {

		
		try {
			BuildIndex.main(new String [] {"-luceneindexpath", luceneIndexPath, 
					"-trainingcycles", Integer.toString(cycles),
					"-termvectorsfile", termModelName, "-docvectorsfile", documentModelName});
		} catch (IllegalArgumentException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	public void buildDRRI(String luceneIndexPath, String documentModelName, String termModelName) {
		try {
			BuildIndex.main(
					new String [] {"-trainingcycles", "2",
					"-luceneindexpath", luceneIndexPath,
					"-termvectorsfile", termModelName, "-docvectorsfile", documentModelName
					});
		} catch (IllegalArgumentException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	/**** PLAIN RI (Document x Term)***/

	public void buildRI(String luceneIndexPath, String documentModelName, String termModelName) {
		try {
			BuildIndex.main(new String[] {"-luceneindexpath", luceneIndexPath, 
					"-termvectorsfile", termModelName, "-docvectorsfile", documentModelName});
		} catch (IllegalArgumentException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/*
	 * 
	 *  POSITIONAL INDEX METHODS...... 
	 * 
	 * */
	
	
	/***** SLIDING WINDOW RI ******/
	
	public void buildSlidingWindow(String luceneIndexPath, String termVectorModelName, int radius) {


		String [] command = {"-luceneindexpath", luceneIndexPath,
				"-windowradius",Integer.toString(radius),
				"-termtermvectorsfile", termVectorModelName, "-docvectorsfile", termVectorModelName +"_docvec", "-elementalvectorfile",
				termVectorModelName +"_elemvec"};

		BuildPositionalIndex.main(command);

	}
	
	public void buildSlidingWindow(String luceneIndexPath, String termVectorModelName) {

		
		String [] command = {"-luceneindexpath", luceneIndexPath,
				"-windowradius","10",
				"-termtermvectorsfile", termVectorModelName, "-docvectorsfile", termVectorModelName +"_docvec", "-elementalvectorfile",
				termVectorModelName +"_elemvec"};

		BuildPositionalIndex.main(command);

	}
	
	/****  HAL   *****/

	public void buildDirectionalHALWindow(String luceneIndexPath, String termVectorModelName, int radius) {


		String [] command = {"-luceneindexpath", luceneIndexPath,
				"-windowradius",Integer.toString(radius),
				"-positionalmethod","directional",
				"-directionalvectorfile", termVectorModelName,
				"-docvectorsfile", termVectorModelName +"_docvec", "-elementalvectorfile",
				termVectorModelName +"_elemvec"};
				
		

		BuildPositionalIndex.main(command);
	}
	
	public void buildDirectionalHALWindow(String luceneIndexPath, String termVectorModelName) {


		String [] command = {"-luceneindexpath", luceneIndexPath,
				"-windowradius","10",
				"-positionalmethod","directional",
				"-directionalvectorfile", termVectorModelName,
				"-docvectorsfile", termVectorModelName +"_docvec", "-elementalvectorfile",
				termVectorModelName +"_elemvec"
		};

		BuildPositionalIndex.main(command);
	}
	
	
	/**** SKIP GRAM  ****/
	
	public void buildSkipgramEmbedding(String luceneIndexPath, String termVectorModelName, int radius) {
		
		String [] command = {"-luceneindexpath", luceneIndexPath,
				"-windowradius",Integer.toString(radius),
				"-positionalmethod","embeddings",
				"-embeddingvectorfile", termVectorModelName,
				"-docvectorsfile", termVectorModelName +"_docvec", "-elementalvectorfile",
				termVectorModelName +"_elemvec"
		};

		BuildPositionalIndex.main(command);
	}
	

	public void buildSkipgramEmbedding(String luceneIndexPath, String termVectorModelName) {
		
		
		String [] command = {"-luceneindexpath", luceneIndexPath,
				"-windowradius","10",
				"-positionalmethod","embeddings",
				"-embeddingvectorfile", termVectorModelName,
				"-docvectorsfile", termVectorModelName +"_docvec", "-elementalvectorfile",
				termVectorModelName +"_elemvec"
		};

		BuildPositionalIndex.main(command);
	}
	
	public static void main(String[] args) throws IllegalArgumentException, IOException {
		// TODO Auto-generated method stub

		
		String luceneLocation = "/Users/mfa/hpv-reddit-2017/";
		
		
		BuildVectorSpaces v = new BuildVectorSpaces();
		
		//Build TxD RI
		v.buildRI(luceneLocation, "hpv_doc_ri", "hpv_term_ri");
		
		//Build TRRI
		v.buildTRRI(luceneLocation, "hpv_doc_trri", "hpv_term_trri");
		
		//Build DRRI
		v.buildDRRI(luceneLocation, "hpv_doc_drri", "hpv_term_drri");
		
		//Build HAL 
		v.buildDirectionalHALWindow(luceneLocation, "hpv_hal_10", 10);
		
		//Build Sliding Window
		v.buildSlidingWindow(luceneLocation, "hpv_dir_10", 10);
		
		//Build Skipgram
		v.buildSkipgramEmbedding(luceneLocation, "hpv_skipgram_10", 10);
		
		
	}

}
