package models;

import com.google.gson.annotations.SerializedName;

public class RedditComment {

    
    @SerializedName("author")
    public String author = "";

    @SerializedName("name")
    public String name = "";

    @SerializedName("body")
    public String body = "";

    @SerializedName("author_flair_text")
    public String author_flair_text = "";

    @SerializedName("gilded")
    public Integer gilded = 0;
    
    @SerializedName("score_hidden")
    public boolean score_hidden;
    
    @SerializedName("score")
    public Integer score = 0;
    
    @SerializedName("link_id")
    public String link_id = "";
    
    @SerializedName("retrieved_on")
    public Long retrieved_on = 0L;
    
    @SerializedName("author_flair_css_class")
    public String author_flair_css_class = "";
    
    @SerializedName("subreddit")
    public String subreddit = "";
    
    @SerializedName("edited")
    public String edited = "";
    
    @SerializedName("ups")
    public Integer ups = 0;
    
    @SerializedName("downs")
    public Integer downs = 0;
    
    @SerializedName("controversiality")
    public Integer controversiality = 0;
    
    @SerializedName("created_utc")
    public Long created_utc = 0L;
    
     @SerializedName("parent_id")
    public String parent_id = "";
     
    @SerializedName("archived")
    public boolean archived;
    
    @SerializedName("subreddit_id")
    public String subreddit_id = "";
    
    @SerializedName("id")
    public String id = "";
    
    @SerializedName("distinguished")
    public String distinguished = "";

}

