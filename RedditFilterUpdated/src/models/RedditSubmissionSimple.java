package models;

import com.google.gson.annotations.SerializedName;

public class RedditSubmissionSimple {

	@SerializedName("id")
	public String id = "";

	//"subreddit":"videos",
	@SerializedName("subreddit")
	public String subreddit= "";

	//"title":"My friend rants about emailing..."
	@SerializedName("title")
	public String title = "";

	//"selftext":"",
	@SerializedName("selftext")
	public String selftext = "";

	@SerializedName("ups")
	public Integer ups = 0;

	@SerializedName("downs")
	public Integer downs = 0;


	@SerializedName("num_comments")
	public Integer num_comments = 0;




}
