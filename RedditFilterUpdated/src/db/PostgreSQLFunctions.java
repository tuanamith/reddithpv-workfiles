package db;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import models.RedditComment;
import models.RedditSubmissionSimple;

public class PostgreSQLFunctions {
	
	Connection conn = null;
    Statement statement = null;
    PreparedStatement pStatement = null;
    ResultSet resultQuery = null;
    int dupes = 0;

    public PostgreSQLFunctions() {
    	
    }

    public PostgreSQLFunctions(String username, String password, String database) {

        try {
            Class.forName("org.postgresql.Driver");
            conn = DriverManager
                    .getConnection("jdbc:postgresql://localhost:5432/" + database,
                            username, password);
            conn.setAutoCommit(false);
            System.out.println("Connection to " + database + " is opened");
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(PostgreSQLFunctions.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(PostgreSQLFunctions.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    
    public void Done() {
        try {
            if (statement != null) {
                if (!statement.isClosed()) {
                    statement.close();
                }
            }

            if (conn != null) {
                if (!conn.isClosed()) {
                    conn.close();
                }
            }

        } catch (SQLException ex) {
            Logger.getLogger(PostgreSQLFunctions.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /* if table does not exist create it*/
    public void verifyTableExists(String tableName) {

        try {
            DatabaseMetaData meta = conn.getMetaData();
            System.out.println(tableName);
            ResultSet resultSet = meta.getTables("public", null, tableName.toLowerCase(), new String[]{"TABLE"});

            if (!resultSet.next()) {
                //create table
            	//System.out.println(resultSet.getString("TABLE_NAME"));
                createTable(tableName);
            }
            resultSet.close();
            //System.exit(0);
            //return false;
        } catch (SQLException ex) {
            Logger.getLogger(PostgreSQLFunctions.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void createTable(String tableName) throws SQLException {
        statement = conn.createStatement();

        String sqlString = "CREATE TABLE " + tableName + " ("
                + "ID TEXT PRIMARY KEY NOT NULL, "
                + "NAME TEXT NOT NULL, "
                + "AUTHOR TEXT, "
                + "BODY TEXT, "
                + "AUTHOR_FLAIR_TEXT TEXT, "
                + "GILDED INT, "
                + "SCORE_HIDDEN BOOLEAN, "
                + "SCORE INT, "
                + "LINK_ID TEXT, "
                + "RETRIEVED_ON BIGINT, "
                + "AUTHOR_FLAIR_CSS_CLASS TEXT, "
                + "SUBREDDIT TEXT, "
                + "EDITED TEXT, "
                + "UPS INT, "
                + "DOWNS INT, "
                + "CONTROVERSIALITY INT, "
                + "CREATED_UTC BIGINT, "
                + "PARENT_ID TEXT, "
                + "ARCHIVED BOOLEAN, "
                + "SUBREDDIT_ID TEXT, "
                + "DISTINGUISHED TEXT)";

        statement.executeUpdate(sqlString);

        //statement.close();
        conn.commit();

    }

    static public void main(String args[]) {
        PostgreSQLFunctions pq = new PostgreSQLFunctions();
    }

    public void addComment() {

        //insertComment();
    }
    
    public boolean doesExist(String index, String dbTable) {
    	boolean result = false;
    	try {
    		String sql = "SELECT COUNT(*) as num from " + dbTable.toLowerCase() + " WHERE id=\'" + index + "\';" ;
    		
			
    		//System.out.println(sql);
    		
    		//System.exit(0);
    		
    		statement = conn.createStatement();
			ResultSet rs =statement.executeQuery(sql);
			//conn.commit();
			rs.next();
			int num =rs.getInt("num");
			//System.out.println(x);
			if(num>0) {
				return true; 
			}
			else {
				return false;
			}
			
			
			/*System.exit(0);
			if(rs.isBeforeFirst()){
				result = true;
			}
			else {
				result = false;
			}*/
			
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	return result;
    }
    
    public void insertSubmissions(String tableName, RedditSubmissionSimple rs) {
    	if(this.doesExist(rs.id, tableName)) {
    		return;
    	}
    	
    	String sql = "INSERT INTO " + tableName.toLowerCase()
    			+ " (ID, SUBREDDIT, TITLE, SELFTEXT, UPS, DOWNS, NUM_COMMENTS) "
    			+ " VALUES (?,?,?,?,?,?,?);";
    	
    	try {
			pStatement = conn.prepareStatement(sql);
			
			pStatement.setString(1, rs.id);
			pStatement.setString(2, rs.subreddit);
			pStatement.setString(3, rs.title);
			pStatement.setString(4, rs.selftext);
			pStatement.setInt(5, rs.ups);
			pStatement.setInt(6, rs.downs);
			pStatement.setInt(7, rs.num_comments);
			
			pStatement.executeUpdate();
			
			//System.out.println(pStatement.toString());
			//System.exit(0);
			
			conn.commit();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    }

    public void insertComment(String tableName, RedditComment rc) throws SQLException {
    	
    	//CHECK IF COMMENT EXIST BEFORE INSERT
    	
    	if(this.doesExist(rc.id, tableName)) {
    		return;
    	}

        try {
            //statement = conn.createStatement();
        	//conn.setAutoCommit(false);
            String sql = "INSERT INTO " + tableName
                    + " (ID, "
                    + "NAME, "
                    + "AUTHOR, "
                    + "BODY, "
                    + "AUTHOR_FLAIR_TEXT, "
                    + "GILDED, "
                    + "SCORE_HIDDEN, "
                    + "SCORE, "
                    + "LINK_ID, "
                    + "RETRIEVED_ON, "
                    + "AUTHOR_FLAIR_CSS_CLASS, "
                    + "SUBREDDIT, "
                    + "EDITED, "
                    + "UPS, "
                    + "DOWNS, "
                    + "CONTROVERSIALITY, "
                    + "CREATED_UTC, "
                    + "PARENT_ID, "
                    + "ARCHIVED, "
                    + "SUBREDDIT_ID, "
                    + "DISTINGUISHED) "
                    + "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";

            pStatement = conn.prepareStatement(sql);
            pStatement.setString(1, rc.id);
            pStatement.setString(2, rc.name);
            pStatement.setString(3, rc.author);
            pStatement.setString(4, rc.body);
            pStatement.setString(5, rc.author_flair_text);
            pStatement.setInt(6, rc.gilded);
            pStatement.setBoolean(7, rc.score_hidden);
            //if(rc.score != null) {pStatement.setInt(8, rc.score);} else {pStatement.setInt(8, 0);}
            pStatement.setInt(8, (rc.score!=null)? rc.score : 0);
            pStatement.setString(9, rc.link_id);
            pStatement.setLong(10, rc.retrieved_on);
            pStatement.setString(11, rc.author_flair_css_class);
            pStatement.setString(12, rc.subreddit);
            pStatement.setString(13, rc.edited);
            pStatement.setInt(14, rc.ups);
            pStatement.setInt(15, rc.downs);
            pStatement.setInt(16, rc.controversiality);
            pStatement.setLong(17, rc.created_utc);
            pStatement.setString(18, rc.parent_id);
            pStatement.setBoolean(19, rc.archived);
            pStatement.setString(20, rc.subreddit_id);
            pStatement.setString(21, rc.distinguished);

            //System.out.println(sql);
            pStatement.executeUpdate();
            //statement.execute(sql);
            //statement.close();

            conn.commit();
            
            

        } catch (SQLException ex) {
            Logger.getLogger(PostgreSQLFunctions.class.getName()).log(Level.SEVERE, null, ex);
            
        }
        
        finally {
        	
        	
        	
        	//conn.setAutoCommit(true);
        }

    }
    
    public void createSentimentTable(String tableName) {

        DatabaseMetaData meta;
        try {
            meta = conn.getMetaData();
            ResultSet resultSet = meta.getTables(null, null, tableName, new String[]{"TABLE"});

            if (resultSet.next()) {
                //create table
                resultSet.close();
                return;
            }
        } catch (SQLException ex) {
            Logger.getLogger(PostgreSQLFunctions.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            statement = conn.createStatement();

            String sqlString = "CREATE TABLE " + tableName + " ("
                    + "ID     SERIAL  PRIMARY KEY UNIQUE NOT NULL, "
                    + "RCID   TEXT    UNIQUE  NOT NULL, "
                    + "AVG_SENT REAL, "
                    + "LONG_SENT INT, "
                    + "TABLE_NAME TEXT "
                    + ")";

            statement.executeUpdate(sqlString);

            //statement.close();
            conn.commit();
        } catch (SQLException ex) {
            Logger.getLogger(PostgreSQLFunctions.class.getName()).log(Level.SEVERE, null, ex);
        } finally {

        }
    }
    
    public boolean isSentimentRecorded(String id, String tableName){
        
        try {
            String sql = "SELECT rcid from " + tableName + " WHERE rcid='" + id + "'";
            statement = conn.createStatement();
            
            if(!statement.executeQuery(sql).next()){
                return false;
            }
            
            
        } catch (SQLException ex) {
            Logger.getLogger(PostgreSQLFunctions.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return true;
    }

    public ResultSet query(String sql) {
        try {
            //ResultSet rs = null;
            statement = conn.createStatement();
            
            resultQuery = statement.executeQuery(sql);

        } catch (SQLException ex) {
            Logger.getLogger(PostgreSQLFunctions.class.getName()).log(Level.SEVERE, null, ex);
        } finally {

            return resultQuery;
        }

    }
    
    public void insertSentimentData(String id, double avg_sentiment, 
            int long_sentiment, String tableOrigin, String sentimentTable ){
        
        String sql = "INSERT INTO " + sentimentTable
                + " (RCID, AVG_SENT, LONG_SENT, TABLE_NAME) VALUES (?,?,?,?);";
        
        try {
            pStatement = conn.prepareCall(sql);
            pStatement.setString(1, id);
            pStatement.setDouble(2, avg_sentiment);
            pStatement.setInt(3, long_sentiment);
            pStatement.setString(4, tableOrigin);
            pStatement.executeUpdate();
            
            conn.commit();
            
        } catch (SQLException ex) {
            Logger.getLogger(PostgreSQLFunctions.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

}
