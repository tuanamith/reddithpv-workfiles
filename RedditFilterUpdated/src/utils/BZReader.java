package utils;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.compress.compressors.CompressorException;
import org.apache.commons.compress.compressors.CompressorInputStream;
import org.apache.commons.compress.compressors.CompressorStreamFactory;

import com.google.gson.Gson;

import models.RedditComment;

public class BZReader {

	static int count = 0;

	static Gson g = new Gson();

	static String fileName = "/Volumes/Macintosh HD Secondary/reddit corpus downloads/reddit_data/2016/RC_2016-06.bz2";

	static public void main(String args[]) {

		System.out.println(fileName);

		File file = new File(fileName);

		if (file.exists() || !file.isDirectory()) {
			System.out.println("file connecting");
		}

		try {
			FileInputStream fileStream = new FileInputStream(file.getAbsoluteFile());

			BufferedInputStream bufferStream = new BufferedInputStream(fileStream);

			CompressorInputStream compressStream = new CompressorStreamFactory().createCompressorInputStream(bufferStream);

			BufferedReader buffer = new BufferedReader(new InputStreamReader(compressStream));

			Gson gson = new Gson();

			buffer.lines().forEach(new Consumer<String>() {
                RedditComment rc = null;
                int count = 0;
                @Override
                public void accept(String line) {
                    rc = gson.fromJson(line, RedditComment.class);
                    if(isHPVRelated(rc.body)){
                        //System.out.println("line: " +gson.toJson(line));


                        rc.body = cleanUpText(rc.body);
                        System.out.println(rc.subreddit);
                        //remove wierd characters
                        count++;
                        System.out.println(count);
                    }

                }
            });
			/*buffer.lines().forEach(line->{
                if(isHPVRelated(gson.fromJson(line, RedditComment.class).body)){

                }
            });*/
			//buffer.lines().forEach((line) -> isHPVRelated(line)); //Slightly faster


			//List<String> hpvstuff = buffer.lines().filter(line -> isHPVRelated(gson.fromJson(line, RedditComment.class).body)).collect(Collectors.toList());

			//System.out.println(hpvstuff.size());


		} catch (FileNotFoundException ex) {
			Logger.getLogger(BZReader.class.getName()).log(Level.SEVERE, null, ex);
		} catch (IOException ex) {
			Logger.getLogger(BZReader.class.getName()).log(Level.SEVERE, null, ex);
		} catch (CompressorException ex) {
			Logger.getLogger(BZReader.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	/*static public RedditComment getHPVComments(String str) {
        if (str.toLowerCase().contains(" hpv ")
                || str.toLowerCase().contains(" papillomavirus ")
                || str.toLowerCase().contains(" cervarix ")
                || str.toLowerCase().contains(" gardasil ")) {

            //System.out.println(count++);
            RedditComment rc = g.fromJson(str, RedditComment.class);

        }
    }*/

	static public boolean isHPVRelated(String str) {

		if (str.toLowerCase().contains(" hpv ")
				|| str.toLowerCase().contains(" papillomavirus ")
				|| str.toLowerCase().contains(" cervarix ")
				|| str.toLowerCase().contains(" gardasil ")) {

			System.out.println(count++);
			return true;

		}

		return false;
	}

	static public boolean isHPVRelated(RedditComment rc) {
		Pattern hpvPattern = Pattern.compile("(?i)\\b(hpv)\\b", Pattern.CASE_INSENSITIVE);
		Matcher matcher;

		matcher = hpvPattern.matcher(rc.body);
		if (matcher.find()) {
			return true;
		}

		Pattern papiPattern = Pattern.compile("(?i)\\b(papillomavirus)\\b", Pattern.CASE_INSENSITIVE);
		matcher = papiPattern.matcher(rc.body);

		if (matcher.find()) {
			return true;
		}

		Pattern ceravix = Pattern.compile("(?i)\\b(Cervarix)\\b", Pattern.CASE_INSENSITIVE);
		matcher = ceravix.matcher(rc.body);

		if (matcher.find()) {
			return true;
		}

		Pattern gardasil = Pattern.compile("(?i)\\b(Gardasil)\\b", Pattern.CASE_INSENSITIVE);
		matcher = gardasil.matcher(rc.body);

		if (matcher.find()) {
			return true;
		}

		return false;
	}

	static public String cleanUpText(String text) {

		text = text.replaceAll("[^\\n\\r\\t\\p{Print}]", ""); //remove POSIX characters
		text = text.replaceAll("https?://\\S+\\s?", ""); // remove urls
		text = text.replaceAll("()", ""); // remove any empty paranethesis as a result of url removal

		return text;
	}

}

