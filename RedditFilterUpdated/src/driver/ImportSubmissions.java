package driver;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.compress.compressors.CompressorException;
import org.apache.commons.compress.compressors.CompressorInputStream;
import org.apache.commons.compress.compressors.CompressorStreamFactory;
import org.apache.commons.io.FilenameUtils;
import org.tukaani.xz.XZInputStream;

import com.google.gson.Gson;

import db.PostgreSQLFunctions;
import models.RedditSubmissionSimple;

public class ImportSubmissions {

	static PostgreSQLFunctions psql = null;

	static Gson gson = new Gson();

	static String dbTable = "";

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		/*use for executing within IDE*/
		args = new String[4];
		args[0] = "/Volumes/Macintosh HD Secondary/reddit corpus downloads/reddit submissions/2017/"; //folder
		args[1] = "mfa";
		args[2] = "";
		args[3] = "reddithpv";
		//args[4] = null;

		//String dbTable = "";

		if (args.length < 4) {
			System.out.println("incorrect arguments");
			return;
		}

		long l1 = System.currentTimeMillis();
		System.out.println("STARTED: " + Long.toString(l1) + "\n");

		//get the folder location (argument 1)
		//** use argument to create table
		//establish connection with database with argument 2 and 3
		psql = new PostgreSQLFunctions(args[1], "", args[3]);

		//collect all files from the folder
		File directory = new File(args[0]);
		File[] files = directory.listFiles((folder, name) -> !name.equals(".DS_Store"));

		dbTable = "subreddits";
		
		for(File file : files){

			System.out.println("Starting " + file.getName() + "....");
			FileInputStream fileStream = null;
			BufferedReader buffer = null;
			try {
				fileStream = new FileInputStream(file.getAbsoluteFile());
				String typeExt = FilenameUtils.getExtension(file.getPath());
				if(!typeExt.equals("xz")) {
					BufferedInputStream bufferStream = new BufferedInputStream(fileStream);
					CompressorInputStream compressStream = new CompressorStreamFactory().createCompressorInputStream(bufferStream);
					buffer = new BufferedReader(new InputStreamReader(compressStream));
				}
				else {
					XZInputStream inxz = new XZInputStream(fileStream);

					buffer = new BufferedReader(new InputStreamReader(inxz));
				}
			

				buffer.lines().forEach((line) -> processHPVRelated(line));

			} catch (FileNotFoundException ex) {
				Logger.getLogger(ImportComments.class.getName()).log(Level.SEVERE, null, ex);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (CompressorException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				try {
					fileStream.close();
				} catch (IOException ex) {
					Logger.getLogger(ImportComments.class.getName()).log(Level.SEVERE, null, ex);
				}
			}

			System.out.println(".... " + file.getName() + " added to database");
		}


		psql.Done();

		System.out.println("Done with : " + args[0]);

		long l2 = System.currentTimeMillis();
		System.out.println("\nENDED: " + Long.toString(l2));
		System.out.println("TOTAL TIME: " + Long.toString((l2 - l1)));

	}

	private static Object processHPVRelated(String str) {
		// TODO Auto-generated method stub
		if (str.toLowerCase().contains(" hpv ")
				|| str.toLowerCase().contains(" papillomavirus ")
				|| str.toLowerCase().contains(" cervarix ")
				|| str.toLowerCase().contains(" gardasil ")) {

			//System.out.println(count++);
			RedditSubmissionSimple rs =gson.fromJson(str, RedditSubmissionSimple.class);

			psql.insertSubmissions(dbTable, rs);

			return true;

		}

		return false;
	}

}
