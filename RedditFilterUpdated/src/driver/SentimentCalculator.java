package driver;

import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.neural.rnn.RNNCoreAnnotations;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.sentiment.SentimentCoreAnnotations;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.util.CoreMap;
import java.util.ArrayList;
import java.util.Properties;
import org.ejml.simple.SimpleMatrix;

public class SentimentCalculator {

	private String matrixString = "";
    private int matrixSize = 0;
    private int final_sentiment = -1; //this is the predicted class based on the longest sentence
    private double avg_sentiment = -1; //sentiment based on average of every sentence
    
    StanfordCoreNLP pipeline = null;
    
    ArrayList<Integer> sentiment_array = new ArrayList<Integer>();
    
    public SentimentCalculator(){
        Properties props = new Properties();
        props.setProperty("annotators", "tokenize, ssplit, parse, sentiment");
        pipeline = new StanfordCoreNLP(props);
    }
    
    public int getMatrixSize() {
        return this.matrixSize;
    }
    
    public void clearData() {
        this.matrixString = "";
        this.final_sentiment = -1;
        this.avg_sentiment = -1;
        this.sentiment_array.clear();
        this.matrixSize = 0;
        //this.text_timer = "";
    }
    
    public String getMatrixString() {
        return this.matrixString.substring(0, matrixString.lastIndexOf("\n"));
    }

    public double getAvgSentiment() {
        return this.avg_sentiment;
    }

    public int getFinalSentiment() {

        return this.final_sentiment;
    }
    
    public void computeSentiment(String text) {
        
        text = text.replaceAll("[^\\n\\r\\t\\p{Print}]", ""); //remove POSIX characters
        text = text.replaceAll("https?://\\S+\\s?", ""); // remove urls
        text = text.replaceAll("()", ""); // remove any empty paranethesis as a result of url removal
        text = text.replaceAll("([.]{2,})", ".");//removes series of periods that interfers with sentiment calculation
        
     int running_length = 0;

        Annotation annotation = pipeline.process(text);
        for (CoreMap sentence : annotation.get(CoreAnnotations.SentencesAnnotation.class)) {
            Tree tree = sentence.get(SentimentCoreAnnotations.SentimentAnnotatedTree.class);

            int sentiment = RNNCoreAnnotations.getPredictedClass(tree);
            String sentenceText = sentence.toString();
            if (sentenceText.length() > running_length) {
                this.final_sentiment = sentiment;
                running_length = sentenceText.length();

            }

            this.sentiment_array.add(sentiment);

            SimpleMatrix sentiment_matrix = RNNCoreAnnotations.getPredictions(tree);

            //add sentiment elements to matrix string
            String matrixStringLine = "";
            for (int i = 0; i < sentiment_matrix.getNumElements(); i++) {
                matrixStringLine = matrixStringLine.concat(sentiment_matrix.get(i) + ",");
            }

            matrixStringLine = matrixStringLine.substring(0, matrixStringLine.lastIndexOf(",") - 1);
            matrixStringLine = matrixStringLine.concat("\n");
            matrixString = matrixString.concat(matrixStringLine);

            matrixSize++;
        }

        //calculate average from sentiment_array
        if (sentiment_array.size() > 1) {
            double temp = 0;
            for (int i : sentiment_array) {
                temp += (double) i;
            }

            this.avg_sentiment = temp / sentiment_array.size();
        } else {
            this.avg_sentiment = this.final_sentiment;
        }

    }
    
    
    static public void main (String args []){
        
        //String testString = "This is some wild shit!";
        //String testString = "Sorry guy but I got the blues today.";
        //String testString = "This is some wild shit! But sorry guy but I got the blues today.";
        //String testString = "Everything I’ve tried to do at Laika, searching for an artful blend of darkness and light, intensity and warmth, humour and heart, I wanted to bring to the Transformers franchise. At its core it’s just a beautiful love story between two broken souls who find each other and heal each other. I grew up loving those classic Spielbergian Amblin films. And we’re trying to evoke that kind of emotion in this movie, fusing a rich coming-of-age story with some sci-fi insanity.";
        
        String testString = "Mixed emotions about it.I’m always eternally grateful to Chris [Nolan]. For instance, ‘Rescue Dawn,’ Werner [Herzog] and I had been trying to put that together for a few years. ‘American Psycho,’ Mary Harron and I had been trying to put that together for a few years. No one was interested. Why? Me. Suddenly everyone said, ‘Yeah, alright. We’ll go with him.’ It did change everything. It was the first time I had done a film of that magnitude. That was a real learning curve for me. I wrestled with it for a long time. I still do on occasions. But I’m just learning, hey, accept the good things.";
        
        
        SentimentCalculator sc = new SentimentCalculator();
        
        sc.computeSentiment(testString);
        
        System.out.println("Final sentiment: " + sc.getFinalSentiment());
        System.out.println("Matrix string: " + sc.getMatrixString());
        System.out.println("Average sentiment: " + sc.getAvgSentiment());
        System.out.println("Matrix size: " + sc.getMatrixSize());
    }
    
}

