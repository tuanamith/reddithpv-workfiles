package driver;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import db.PostgreSQLFunctions;

public class MeasureRedditSentiment {

	static SentimentCalculator sc = null;
    static PostgreSQLFunctions psql = null;
    static String sentimentTableName = "";
    
    static final DateFormat format = new SimpleDateFormat("HHmmss_MMddyyyy");

    static public void main(String args[]) {

        /*use for executing within IDE*/
        //args = new String[4];
        //args[0] = "mfamith";
        //args[1] = "";
        //args[2] = "reddit_hpv";
        //args[3] = "testtable_";
        
        if(args.length != 4){
            System.out.println("Something is wrong with arguments");
            return;
        }
        
        Date now = new Date();
        long l1 = System.currentTimeMillis();
        System.out.println("STARTED: " + Long.toString(l1) + "\n");

        sc = new SentimentCalculator();

        psql = new PostgreSQLFunctions(args[0], "", args[2]);

        sentimentTableName = args[3] + "_" + format.format(now).toString();
        //sentimentTableName = "comments_sentiment_235645_12242017"; //for mutiple
        psql.createSentimentTable(sentimentTableName);

        String[] targetTables = {"rc_2007", "rc_2008", "rc_2009", "rc_2010", "rc_2011", "rc_2012", "rc_2013", "rc_2014", "rc_2015", "rc_2016"};
        //String[] targetTables = {"rc_2016"}; //for multiple instance runs
        String sql_string = "SELECT id, body from ";
        double avg_sentiment = -1;
        int long_sentiment = -1;

        for (int i = 0; i < targetTables.length; i++) {
            System.out.println(targetTables[i] + "....");
            
            ResultSet rs = psql.query(sql_string + targetTables[i] + " ORDER BY ID DESC;"); //added order by

            try {

                while (rs.next()) {

                    String body = rs.getString("body");
                    String id = rs.getString("id");
                    sc.computeSentiment(body);

                    avg_sentiment = sc.getAvgSentiment();
                    long_sentiment = sc.getFinalSentiment();

                    psql.insertSentimentData(id, avg_sentiment, long_sentiment, targetTables[i], sentimentTableName);
                    sc.clearData();

                }

            } catch (SQLException ex) {
                Logger.getLogger(MeasureRedditSentiment.class.getName()).log(Level.SEVERE, null, ex);
            }

        }

        
        
        psql.Done();
        long l2 = System.currentTimeMillis();
        System.out.println("\nENDED: " + Long.toString(l2));
        System.out.println("TOTAL TIME: " + Long.toString((l2 - l1)));

    }

}
