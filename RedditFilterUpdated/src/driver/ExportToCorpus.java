package driver;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.commons.text.StringEscapeUtils;

import db.PostgreSQLFunctions;

public class ExportToCorpus {

	static PostgreSQLFunctions psql = null;

	public ExportToCorpus() {
		// TODO Auto-generated constructor stub
	}

	public void submissionsDump(String sql, String folder) {
		System.out.println("Dumping submissions...");
		ResultSet rs = psql.query(sql);
		File file = null;

		try {
			while (rs.next()) {
				file = new File(folder + rs.getString("id") + ".txt");
				try (FileOutputStream fos = new FileOutputStream(file)) {

					String contents = (rs.getString("selftext")!=null) ? 
							rs.getString("title") + "\n" + rs.getString("selftext") : rs.getString("title");
							
							contents = StringEscapeUtils.unescapeJava(contents);
							
							byte[] contentInBytes = contents.getBytes();
							fos.write(contentInBytes);
							fos.flush();
							fos.close();

				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println(".... Finished.");
	}

	public void commentsDump(String sql, String folder) {
		System.out.println("Dumping comments...");
		ResultSet rs = psql.query(sql);
		File file = null;

		try {
			while (rs.next()) {
				file = new File(folder + rs.getString("id") + ".txt");
				try (FileOutputStream fos = new FileOutputStream(file)) {
					byte[] contentInBytes = rs.getString("body").getBytes();
					fos.write(contentInBytes);
					fos.flush();
					fos.close();

				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println(".... Finished.");
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		String role = "mfa";
		String database = "reddithpv";

		long l1 = System.currentTimeMillis();
		System.out.println("STARTED: " + Long.toString(l1) + "\n");

		psql = new PostgreSQLFunctions(role, "", database);

		ExportToCorpus ec = new ExportToCorpus();

		ec.commentsDump("SELECT id, body from rc_corpus", "/Users/mfa/reddit_corpus_07_17/");

		ec.submissionsDump("SELECT id, title, selftext from rs_corpus", "/Users/mfa/reddit_corpus_07_17/");

		psql.Done();
		long l2 = System.currentTimeMillis();
		System.out.println("\nENDED: " + Long.toString(l2));
		System.out.println("TOTAL TIME: " + Long.toString((l2 - l1)));
	}

}
