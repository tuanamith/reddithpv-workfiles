import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;

import org.apache.commons.io.FileUtils;

public class SplitCorpus {
	
	static public File combinedFile = new File("/Users/mfa/word2vec/processed_reddit.txt");
	static public File outputDirectory = new File("/Users/mfa/reddit_corpus_07_17_processed/");
	static public String delimiter = "\\*\\*\\*\\*\\*\\*QAPLA\\*\\*\\*\\*\\*\\*"; //Klingon for success
	int count = 1;
	public SplitCorpus() {
		// TODO Auto-generated constructor stub
		InputStream input;
		try {
			input = new FileInputStream(combinedFile.getAbsolutePath());
			Scanner scanner = new Scanner(input);
			scanner.useDelimiter(delimiter);
			while (scanner.hasNext()) {
				//System.out.println(scanner.next().trim());
				this.saveFile(scanner.next().trim());
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	private void saveFile(String content) {
		File outputFile = new File(outputDirectory.getAbsolutePath()+ "/" + count +".txt");
		
		System.out.println(outputFile.getAbsolutePath());
		
		
		try {
			FileUtils.writeStringToFile(outputFile, content, StandardCharsets.UTF_8.name());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		count++;
		System.out.print(count+" files saved \r");
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		SplitCorpus sc = new SplitCorpus();

	}

}
