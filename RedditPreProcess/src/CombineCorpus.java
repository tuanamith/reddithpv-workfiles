import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

import org.apache.commons.io.FileUtils;

public class CombineCorpus {
	
	static String source = "/Users/mfa/reddit_corpus_07_17/";
	static String output_source = "/Users/mfa/combined_reddit.txt";
	
	static public File outputFile = new File(output_source);
	static public File directory = new File(source);
	boolean isFirstFile = true;
	static public String delimiter = "\n\n\n******QAPLA******\n\n\n"; //Klingon for success
	public CombineCorpus() {
		// TODO Auto-generated constructor stub
		
		
	}
	
	private void appendToCorpus(String content) {
		if(!isFirstFile) {
			content = delimiter + content;
		}
		else {
			isFirstFile = false;
		}
		
		try {
			//FileUtils.write(outputFile, content, true);
			FileUtils.write(outputFile, content,
					StandardCharsets.UTF_8, true);
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	public void processFile(File file) {
		//File targetFile = null;
		
		try {
			String content = FileUtils.readFileToString(file, StandardCharsets.UTF_8);
			this.appendToCorpus(content);
			
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//return targetFile;
	}
	

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Starting...");
		CombineCorpus cc = new CombineCorpus();
		
		File [] files = directory.listFiles(new FilenameFilter() {
		    public boolean accept(File dir, String name) {
		        return name.toLowerCase().endsWith(".txt");
		    }
		});
		int count = 0;
		for(File file : files) {
			//System.out.println(file.getAbsolutePath());
			cc.processFile(file);
			count++;
			System.out.print(count +" documents processed... \r");
		}
		System.out.println("Done");
	}

}
